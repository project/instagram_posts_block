
INTRODUCTION:
This module offers a feature that allows you to retrieve image posts associated with an Instagram account. To learn more about the module, please visit the project page:

https://www.drupal.org/project/instagram_posts_block

REQUIREMENTS:
To install the library, it is recommended to use composer. Run the following command:

composer require drupal/instagram_posts_block

INSTALLATION:
Once the module is downloaded,

Navigate to the Extend section and enable the module.

CONFIGURATION:
After installing the module,
you will find a new block called "Instagram Posts Block".

Go to Structure -> Block layout, and click on "Place block" in the desired region.

In the block modal's search bar, search for "Instagram Posts Block" and select it.

The newly added block will provide options to add the access token and adjust other settings to control the display of the Instagram feed.

Author:
This module, "instagram_posts_block," was developed by Ahmad Abbad.