<?php

namespace Drupal\instagram_posts_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Instagram Posts' block.
 *
 * @Block(
 *   id = "instagram_posts_block",
 *   admin_label = @Translation("Instagram Posts Block"),
 * )
 */
class InstagramPostsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('http_client')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $images_per_row = $config['images_per_row'];
    $media_type = $config['media_type'];
    $trimmed_limit = $config['trimmed_limit'];
    $caption_option = $config['caption_option'];

    $build = [
      '#theme' => 'instagram_posts',
      '#posts' => [],
      '#images_per_row' => $images_per_row,
      '#media_type' => $media_type,
      '#trimmed_limit' => $trimmed_limit,
      '#caption_option' => $caption_option,
    ];

    try {
      $access_token = $config['access_token'];
      $total_images = $config['total_images'];

      // Send request to instagram to fetch the feed.
      $request = $this->httpClient->get('https://graph.instagram.com/me/media', [
        'query' => [
          'fields' => 'id,media_url,media_type,permalink,caption',
          'access_token' => $access_token,
          'limit' => $total_images,
        ],
      ]);
    }
    catch (RequestException $e) {
      $logger = \Drupal::logger('HTTP Client error');
      $logger->error($e->getMessage());
      return;
    }

    if ($request->getStatusCode() != 200) {
      return $build;
    }

    $results = $request->getBody()->getContents();
    $posts = json_decode($results)->data;

    foreach ($posts as $post) {
      $build['#posts'][] = [
        'media_url'  => $post->media_url,
        'permalink'  => $post->permalink,
        'media_type' => $post->media_type,
        'caption'    => $post->caption ?? '',
      ];
    }

    if (!empty($build)) {
      $build['#attached']['library'][] = 'instagram_posts_block/instagram_posts_block';
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['total_images'] = [
      '#type' => 'number',
      '#title' => $this->t('Total no. of images to display'),
      '#description' => $this->t('How many images in total to be displayed'),
      '#min' => 1,
      '#default_value' => isset($config['total_images']) ? $config['total_images'] : '',
      '#required' => TRUE,
    ];
    $form['images_per_row'] = [
      '#type' => 'number',
      '#title' => $this->t('No. of images per row'),
      '#min' => 1,
      '#description' => $this->t('How many images to be displayed per row?'),
      '#default_value' => isset($config['images_per_row']) ? $config['images_per_row'] : '',
      '#required' => TRUE,
    ];
    $form['media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Media type'),
      '#description' => $this->t('Select the type of media to be displayed'),
      '#options' => [
        'All' => $this->t('All'),
        'IMAGE' => $this->t('Image'),
        'VIDEO' => $this->t('Video'),
      ],
      '#default_value' => isset($config['media_type']) ? $config['media_type'] : 'All',
      '#required' => TRUE,
    ];
    $form['caption_option'] = [
      '#type' => 'select',
      '#title' => $this->t('Caption format'),
      '#options' => [
        'default' => $this->t('Default'),
        'trimmed' => $this->t('Trimmed'),
      ],
      '#default_value' => isset($config['caption_option']) ? $config['caption_option'] : 'default',
      '#attributes' => [
        'id' => 'field_caption',
      ],
    ];
    $form['trimmed_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Trimmed limit'),
      '#field_suffix' => $this->t('characters'),
      '#min' => 1,
      '#default_value' => isset($config['trimmed_limit']) ? $config['trimmed_limit'] : 100,
      '#states' => [
        'visible' => [
          ':input[id="field_caption"]' => ['value' => 'trimmed'],
        ],
      ],
    ];
    $form['access_token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Long Lived Access Token'),
      '#description' => $this->t('Access Token to access insta feed?'),
      '#default_value' => isset($config['access_token']) ? $config['access_token'] : '',
      '#required' => TRUE,
    ];
    $form['token_timestamp'] = [
      '#title' => $this->t('The time when the Access Token was created'),
      '#type' => 'textfield',
      '#default_value' => isset($config['token_timestamp']) ? $config['token_timestamp'] : time(),
      '#disabled' => 'TRUE',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['total_images'] = $values['total_images'];
    $this->configuration['images_per_row'] = $values['images_per_row'];
    $this->configuration['media_type'] = $values['media_type'];
    $this->configuration['caption_option'] = $values['caption_option'];
    $this->configuration['trimmed_limit'] = $values['trimmed_limit'];
    $this->configuration['access_token'] = $values['access_token'];
    $this->configuration['token_timestamp'] = $values['token_timestamp'];
  }

}
